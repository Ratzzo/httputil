#ifndef DEBUG_H
#define DEBUG_H

#ifdef _DEBUG_

void *mmalloc(const char *file, int line, int size);
void mfree(const char *file, int line, void *ptr);
void *mrealloc(const char *file, int line, void *ptr, int size);
void mallocreport();

#define malloc(...) mmalloc(__FILE__,__LINE__,__VA_ARGS__);
#define free(...) mfree(__FILE__,__LINE__,__VA_ARGS__);
#define realloc(...) mrealloc(__FILE__,__LINE__,__VA_ARGS__);

#define dprintf(...) printf(__VA_ARGS__);

#else
#define mallocreport()
#endif

#endif // DEBUG_H
