#define CURL_STATICLIB

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <apr.h>
#include <apr_pools.h>
#include <vector>

#include "httputil.h"


const char *urls[] = {
"https://41.media.tumblr.com/38d7c7c9c333c1cbf5994ba29a9e05a1/tumblr_ndia14IJzh1qzjc9co1_1280.jpg",
"https://41.media.tumblr.com/9d450954a76645406d982569ca716eec/tumblr_njbej0oKOr1rl0ynao1_400.jpg",
"https://40.media.tumblr.com/440aead4b2cd318243bc3793942ef0ba/tumblr_nj6jsb40Fc1s4kjkoo1_540.jpg",
"https://onedrive.live.com/download?cid=7997A3844638A2FE&resid=7997A3844638A2FE%21318&authkey=AAkNmdTQ4gaWIDQ"
};


int main(void)
{
    if(apr_initialize()) return 1;
    curl_global_init(CURL_GLOBAL_ALL);
    std::vector<hutil_downdata> dda;

    hutil_initialize();

    //preinitialize dda
    for(int i = 0; i < sizeof(urls)/sizeof(const char*); i++){
        hutil_downdata dd;
        dda.push_back(dd);
    }

    //perform the request as nonblocking
    for(int i = 0; i < dda.size(); i++){
    hutil_performasyncrequest(urls[i], &dda[i]);
    }


    while(1){
        int finished = 1;
        apr_sleep(100000);
        printf("----------------------------------------\n");
        for(int i = 0; i < dda.size(); i++){
        finished &= dda[i].finished;
        printf("dd[%i]->finished = %i\n", i, dda[i].finished);
        printf("dd[%i]->progress = %i\n", i, dda[i].progress);
        }
        if(finished) break;
    }

    for(int i = 0; i < dda.size(); i++){

    }

    hutil_terminate();

  /* we're done with libcurl, so clean it up */
  curl_global_cleanup();
  apr_terminate();

  return 0;
}
