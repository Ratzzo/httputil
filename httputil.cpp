#include "httputil.h"
#include "main.h"

int _hutil_allocate_pool(hutil_t *hu){

    if(apr_pool_create(&hu->hutil_pool, 0)) return 1;
    apr_allocator_t *alloc = apr_pool_allocator_get(hu->hutil_pool);
    if(alloc) apr_allocator_max_free_set(alloc, 32);
    else return 1;
    return 0;
}

int hutil_initialize(hutil_t *hu)
{
    int rv = 0;
    hu->curl_handle = curl_easy_init();
    if(!hu->curl_handle) return 1;
    rv = _hutil_allocate_pool(hu);
    if(!rv) return 2;
    return 0;
}

int hutil_terminate(hutil_t *hu)
{
    curl_easy_cleanup(hu->curl_handle);
    apr_pool_destroy(hu->hutil_pool);
    return 0;
}

typedef struct
{
    const char *url;
    char *postfields;
    hutil_downdata_t *dd;
    hutil_t *hu;
} hutil_threadarguments_t;

void* APR_THREAD_FUNC hutil_downthread(apr_thread_t *thd, void *data)
{

    hutil_threadarguments_t *args = (hutil_threadarguments_t*)data;
     //printf("trying to hutil_performblockingrequest();\n");
     args->dd->rval = -1;
    int result = hutil_performblockingrequest(args->hu, args->url, args->dd, args->postfields);
    if(result != CURLE_OK){
        apr_sleep(1000000);
    }
    args->dd->rval = result;
   // _hutil_allocs++;
    //printf("args->dd->rval = %i\n", args->dd->rval);
//    *((int*)0) = 1;
    return (void*)(args->dd->rval);

}

//progress callback
int hutil_pcallback(void *clientp, curl_off_t dltotal, curl_off_t dlnow, curl_off_t ultotal, curl_off_t ulnow)
{
    hutil_downdata_t *dd = (hutil_downdata_t*)clientp;
    dd->progress = dlnow;
    dd->expectedtotal = ultotal;
    if(dd->forcetermination) return 1;
//    printf("%u of %u\n", dlnow, ultotal);
    return 0;
}

static size_t hutil_wcallback(void *retrieved, size_t size, size_t nmemb, void *userp)
{
    size_t rsize = size * nmemb;

    hutil_downdata_t *dd = (hutil_downdata_t*)userp;

    char *temp = (char*)realloc(dd->data, dd->size + rsize);
    if(!temp) return 0;
    dd->data = temp;

    memcpy(dd->data + dd->size, retrieved, rsize);
    dd->size += rsize;
    if(dd->forcetermination) {
        free(dd->data);
        dd->data = 0;
        return 0;
    }
    return rsize;
}

int hutil_sockopt_callback(void *clientp, curl_socket_t curlfd, curlsocktype purpose)
{
    int i = 1;
    setsockopt(curlfd, SOL_SOCKET, SO_REUSEADDR, (const char*)&i, sizeof(int));
    return CURL_SOCKOPT_OK;
}

int hutil_performblockingrequest(hutil_t *hu, const char *url, hutil_downdata_t *dd, char *postfields)
{
    CURLcode res;
//    struct curl_slist *cs;


    dd->data = (char*)malloc(1);  /* will be grown as needed by the realloc above */
    dd->size = 0;    /* no data at this point */
    dd->progress = 0;
    dd->finished = 0;
    dd->expectedtotal = 0;
    dd->forcetermination = 0;

    /* init the curl session */
    curl_easy_setopt(hu->curl_handle, CURLOPT_CAINFO, HUTIL_CACERTPATH);
    curl_easy_setopt(hu->curl_handle, CURLOPT_FOLLOWLOCATION, 1);
    curl_easy_setopt(hu->curl_handle, CURLOPT_NOPROGRESS, 0);
    curl_easy_setopt(hu->curl_handle, CURLOPT_SOCKOPTFUNCTION, hutil_sockopt_callback);
    /* specify URL to get */
    curl_easy_setopt(hu->curl_handle, CURLOPT_URL, url);

    if(postfields)
    curl_easy_setopt(hu->curl_handle, CURLOPT_POSTFIELDS, postfields);

    curl_easy_setopt(hu->curl_handle, CURLOPT_WRITEFUNCTION, hutil_wcallback);
    curl_easy_setopt(hu->curl_handle, CURLOPT_XFERINFOFUNCTION, hutil_pcallback);

    /* we pass our 'chunk' struct to the callback function */
    curl_easy_setopt(hu->curl_handle, CURLOPT_WRITEDATA, (void*)dd);
    curl_easy_setopt(hu->curl_handle, CURLOPT_XFERINFODATA, (void*)dd);

    /* some servers don't like requests that are made without a user-agent
       field, so we provide one */
    curl_easy_setopt(hu->curl_handle, CURLOPT_USERAGENT, "libcurl-agent/1.0");

    /* get it! */
    res = curl_easy_perform(hu->curl_handle);

    /* check for errors */
    int rv = 0;
    if(res != CURLE_OK)
        rv = res;

    if(rv)
        dd->finished = res;
    else
        dd->finished = 1;

    return rv;
}

int hutil_performasyncrequest(hutil_t *hu, const char *url, hutil_downdata_t *dd, char *postfields)
{
    //starts a new thread
    if(hu->_hutil_allocs > HUTIL_POOLCLEANUP && hu->hutil_pool){
    apr_pool_destroy(hu->hutil_pool);
    _hutil_allocate_pool(hu);
    hu->_hutil_allocs = 0;
    }
    hu->_hutil_allocs++;
    memset(dd, 0, sizeof(hutil_downdata_t));
    hutil_threadarguments_t *args = (hutil_threadarguments_t*)apr_palloc(hu->hutil_pool, sizeof(hutil_threadarguments_t));
    args->dd = dd;
    args->url = url;
    args->postfields = postfields;
    args->hu = hu;
    apr_threadattr_t *attr;
    apr_thread_t *thread;
    if(apr_threadattr_create(&attr, hu->hutil_pool)) return 1;
    if(apr_thread_create(&thread, attr, hutil_downthread, (void*)args, hu->hutil_pool)) return 1;
    return 0;
}
int hutil_rsapublicencrypt(char *der, int derlen, const unsigned char *in, int insize, datasize32_t *out)
{
    BIO *buffio = 0;
    RSA *rsa = 0;
    int rsa_size;
    int rv = 0;
    unsigned char *outbuffer = 0;

    buffio = BIO_new_mem_buf(der, derlen);
    if(!buffio) return 1;
    if(!d2i_RSA_PUBKEY_bio(buffio, &rsa)){
        rv = 2;
        goto finalize;
    }
    rsa_size = RSA_size(rsa);
    outbuffer = (unsigned char*)malloc(rsa_size);
    if(!outbuffer) { rv = 3; goto finalize2;}
    memset(outbuffer, 0, rsa_size);

    RSA_public_encrypt(insize, (const unsigned char*)in, outbuffer, rsa, /*RSA_PKCS1_OAEP_PADDING*/ RSA_PKCS1_OAEP_PADDING);
    out->data = (char*)outbuffer;
    out->size = rsa_size;

    //printf("%u\n", rsa->version);
//       RSA_public_encrypt()
    finalize2:
    RSA_free(rsa);
    finalize:
    BIO_destroy_bio_pair(buffio);
    return rv;
}

char *hutil_base64rsapublicencrypt(char *der, int derlen, const char *in, int insize){
    datasize32_t ds = {};
   if(hutil_rsapublicencrypt(der, derlen, (const unsigned char*)in, insize, &ds)) return 0;
   int newsize = apr_base64_encode_len(ds.size);
   char *result = (char*)malloc(newsize+1);
   if(!result) { free(ds.data);return 0;}
   apr_base64_encode(result, ds.data, ds.size);
   result[newsize] = 0;
   free(ds.data);
   return result;
}

char *hutil_urlencodedbase64rsapublicencrypt(char *der, int derlen, const char *in, int insize){
    char *temp, *tofree;
    if(!(temp = hutil_urlencode(tofree = hutil_base64rsapublicencrypt(der, derlen, in, insize)))) return 0;
    free(tofree);
    return temp;
}

unsigned char *hutil_bfencrypt(BF_KEY *key, unsigned char *buffer, unsigned int buffersize, unsigned char *iv, int *len){
//    int len = 0;
    unsigned char *res = (unsigned char*)malloc(buffersize);
    BF_ofb64_encrypt(buffer, res, buffersize, key, iv, len);
   // res = (unsigned char*)realloc(res, *len);
    return res;
}

char *hutil_base64bfencrypt(BF_KEY *key, unsigned char *buffer, unsigned int buffersize, unsigned char *iv){
    int len = 0;
    unsigned char *res = 0;
   if(!(res = hutil_bfencrypt(key, buffer, buffersize, iv, &len))) return 0;
//   printf("len = %u\n", len);
   int newsize = apr_base64_encode_len(buffersize);
   char *result = (char*)malloc(newsize+1);
   if(!result) { free(res);return 0;}
   apr_base64_encode(result, (const char*)res, buffersize);
   result[newsize] = 0;
   free(res);
   return result;

}

void hutil_hexdump(unsigned char *buff, int size){
    printf("----------------------------------------------\n");
    for(int i = 0; i < size; i++){
        printf("%2X, ", buff[i]);
        if(!((i+1) % 15)) printf("\n");
    }
    printf("\n");
    printf("----------------------------------------------\n");
}

char *hutil_urlencodedbase64bfencrypt(BF_KEY *key, unsigned char *buffer, unsigned int buffersize, unsigned char *iv){
    char *temp, *tofree;
    if(!(temp = hutil_urlencode(tofree = hutil_base64bfencrypt(key, buffer, buffersize, iv)))) return 0;
    free(tofree);
    return temp;
}

typedef struct
{
    unsigned int size;
    const char **chunks;
} aux_escapestring_array_t;

//returned string needs to be freed after calling this function
char *hutil_urlencode(const char *str)
{
    if(!str) return 0;
    int spacing = 3;
    char characters[] = {'/', '+', '='}; //characters to scape
    aux_escapestring_array_t c_arr = {0,0}; //chunks array
    int isescapable = 0;
    if(!str) return 0;
    int newstrlen = strlen(str) + 1;
    int n = 0;
    spacing--;

    while(str[n])
    {
        for(int i = 0; i < (int)sizeof(characters); i++)
            if(str[n] == characters[i])
            {
                isescapable = 1;
                break;
            }
        if(isescapable)
        {
            newstrlen += spacing; //add space for the %xx
            c_arr.size++;
            c_arr.chunks = (const char**)realloc(c_arr.chunks, sizeof(const char*)*c_arr.size);
            c_arr.chunks[c_arr.size-1] = &str[n];
            if(!c_arr.chunks) return 0;
        }
        isescapable = 0;
        n++;
    }

    char *newstr = (char*)malloc(newstrlen);
    if(!newstr){
        free(c_arr.chunks);
        return 0;
    }
    memset(newstr, 0, newstrlen);
    strcpy(newstr, str);

    char hex[3];
    hex[2] = 0;
    for(unsigned int i = 0; i < c_arr.size; i++)
    {
        sprintf(hex, "%.2X", c_arr.chunks[i][0]);
        memcpy((newstr + (c_arr.chunks[i] - str)) + i*spacing + spacing, c_arr.chunks[i], strlen(c_arr.chunks[i]));
        *(newstr + (c_arr.chunks[i] - str) + i*spacing) = '%';
        *(newstr + (c_arr.chunks[i] - str) + i*spacing+1) = hex[0];
        *(newstr + (c_arr.chunks[i] - str) + i*spacing+2) = hex[1];
    }
    free(c_arr.chunks);
    return newstr;
}
