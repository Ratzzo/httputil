#define CURL_STATICLIB

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <apr.h>
#include <apr_pools.h>
#include <vector>
#include "base64/apr_base64.h"
#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/bio.h>

#include "httputil.h"

#include "userauth.h"
#include "main.h"
//#include "debug.h"
extern char public_key_der[550];

#define gotcha() {printf("LINE:%u", __LINE__); goto error;}

#define _DEF_HOST_ "127.0.0.1"

int ix = 0;

int bcb(hutil_downdata_t *dd){
   // printf("dd.finished %i\n", dd->expectedtotal);
//    if(dd->finished) printf("Finished!");
    apr_sleep(1000);
  //printf("bcb\n");
//    ix++;
    return 0;
}

int main(void)
{
    int rv = 0;
    if(apr_initialize()) return 1;
    curl_global_init(CURL_GLOBAL_ALL);
    hutil_t hu;
    hutil_initialize(&hu);
    {

       printf("testing...\n");
       UserAuth hs(&hu, "http://" _DEF_HOST_ "/httputil/php/");
       hs.SetPublicKey(public_key_der, sizeof(public_key_der));
       hs.busycallback = bcb;

        printf("Starting session... ");
       if((rv = hs.StartSession())) gotcha();
       printf("OK.\n");
        {

        }


        {
           hsec_response_t resp;


           /// this test send the variable incme to the server which adds 1 and sends it back. this is done 100 times as a ping/pong test
            uint32_t incme = 0;
            int ret = 0;

            printf("Authenticating... ");
            if((rv = hs.authenticate("user", "password"))) gotcha();
            printf("OK.\n");
            printf("Authenticated as %s\n", hs.ad.username.c_str());
    for(int m = 0; m < 100; m++){
       // hs.StartSession();
       if(!(ret = hs.SendData(0xfe, 100, (char*)&incme, sizeof(incme), &resp))){
        incme = *(unsigned int*)resp.data;
       //printf("resp.data = %s\n", resp.data);
       //printf("needskeychange = %u\n", resp.code);
       printf("incme = %u\n", incme);
 //      free(resp.pointer);
       //apr_sleep(100000);
       }
       else
        printf("Failed! %u\n", ret);
    }
       }

        printf("Terminating session... ");
       if(hs.EndSession()) gotcha();
       printf("OK.\n");


       goto noerror;
       error:
       printf("Failed. return code %u lasterror = %i\n", rv, hs.lasthserror);
       noerror:;
    }

    mallocreport();
//    apr_sleep(3000000);

    hutil_terminate(&hu);

  /* we're done with libcurl, so clean it up */
  curl_global_cleanup();

 //     apr_sleep(3000000);
  apr_terminate();

  return 0;
}
