#include <stdio.h>
#include <stdlib.h>
#include <vector>

typedef struct {
    void *address;
    int freed;
    const char *allocfname;
    const char *freefname;
    int allocline;
    int freeline;
} ialloc_t; //individual alloc

std::vector<ialloc_t> allocs;

void *mmalloc(const char *file, int line, int size){
    void *rv = malloc(size);
    ialloc_t al;
    al.address = rv;
    al.freed = 0;
    al.allocfname = file;
    al.freefname = 0;
    al.allocline = line;
    al.freeline = 0;
    allocs.push_back(al);
    // printf("malloced %x; %s:%i\n", rv, file, line);1
     return rv;
}

void mfree(const char *file, int line, void *ptr){
    //printf("freed %x; %s:%i\n", ptr, file, line);
    for(unsigned int i = 0; i < allocs.size(); i++){
        if(allocs[i].address == ptr)
        {
            if(allocs[i].freed) printf("double free %x; %s:%i\nLast free: %s:%i", (unsigned int)ptr, file, line, allocs[i].freefname, allocs[i].freeline);
            allocs[i].freed = 1;
            allocs[i].freeline = line;
            allocs[i].freefname = file;
            break;
        }
    }
    free(ptr);
}

void *mrealloc(const char *file, int line, void *ptr, int size){
    void *rv = realloc(ptr, size);
    //printf("realloc %x to %x; %s:%i\n", ptr, rv, file, line);
    for(unsigned int i = 0; i < allocs.size(); i++)
        if(allocs[i].address == ptr && !allocs[i].freed)
            allocs[i].address = rv;

    return rv;
}

void mallocreport(){
    int showallocfaults = 0;
    for(unsigned int i = 0; i < allocs.size(); i++)
        if(!allocs[i].freed)
            showallocfaults = 1;

    if(showallocfaults){
    printf("-------------------------------------------------------\n");
    for(unsigned int i = 0; i < allocs.size(); i++){
        if(!allocs[i].freed)
            printf("Memory leak: %x; %s:%i\n", (unsigned int)allocs[i].address, allocs[i].allocfname, allocs[i].allocline);
    }
    printf("-------------------------------------------------------\n");
    }
    else
    printf("No allocation errors found, nice!\n");
}

