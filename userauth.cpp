#include "userauth.h"
#include "main.h"

uint16_t hs_authenticate = hs_last + 1;
uint16_t hs_getrelevantdata = hs_last +2 ;

UserAuth::UserAuth(hutil_t *hu, const char *baseurl, const char *sessioninitiator, const char *comhandler): HSec(hu, baseurl, sessioninitiator, comhandler){
	authenticated = 0;
	ad.username.assign("");
	ad.email.assign("");
	ad.id = -1;
}

extern "C" {

//copies <size> bytes of memory from <src> at <*srcoffset> (which is then incremented by size) to <dest> only if they don't exceed either the <maxsrcsize> or <maxdestsize>
inline int safememcpy(void *dest, void *src, uint32_t size, uint32_t *srcoffset, uint32_t maxsrcsize, uint32_t maxdestsize = 0){
    if((maxsrcsize && (size + *srcoffset > maxsrcsize)) || (maxdestsize && (size > maxdestsize))) return 1;
    memcpy(dest, (uint8_t*)src + *srcoffset, size);
    *srcoffset += size;
    return 0;
}

}



//retrieve account data
int UserAuth::getAccountData(){
    if(!authenticated) return 1;
    char buffer[256];
    uint32_t off = 0;
    hsec_response_t resp;

    //data to get
    int32_t accid; //account id
    uint16_t username_size; //username string size
    uint16_t email_size; //email string size
    uint8_t account_type;
    std::string username; //temporary username string
    std::string email; //temporary email string

    int rv;
    if((rv = SendData(hs_getrelevantdata, 256, "", 0, &resp))) return rv;

    //get account id
    if(safememcpy(&accid, resp.data, sizeof(accid), &off, resp.length)) return 2;

    //get username size
    if(safememcpy(&username_size, resp.data, sizeof(username_size), &off, resp.length)) return 3;

    //get username
    memset(buffer, 0, sizeof(buffer));
    if(safememcpy(buffer, resp.data, username_size, &off, resp.length, sizeof(buffer)-1)) return 4;
    username.assign(buffer);

    //get email size
    if(safememcpy(&email_size, resp.data, sizeof(email_size), &off, resp.length)) return 5;

    //get email
    memset(buffer, 0, sizeof(buffer));
    if(safememcpy(buffer, resp.data, email_size, &off, resp.length, sizeof(buffer)-1)) return 6;
    email.assign(buffer);

    //get account type
    if(safememcpy(&account_type, resp.data, sizeof(account_type), &off, resp.length)) return 7;


    //finish
    ad.id = accid;
    ad.type = account_type;
    ad.username.assign(username);
    ad.email.assign(email);

    dprint("\n");
    dprint("id: %u\n", ad.id);
    dprint("user: %s\n", ad.username.c_str());
    dprint("email: %s\n", ad.email.c_str());
    dprint("type: %i\n", ad.type);

    return 0;
}

int UserAuth::authenticate(const char *user, const char *password){
    int ulen = 0, plen = 0;
    if((ulen = strlen(user)) > 128) return 1;
    if((plen = strlen(password)) > 128) return 2;
    char buffer[1024];
    char *ptr = buffer;

    authenticated = 0;


    //encode
    *(uint16_t*)ptr = ulen;//ulen;
    memcpy(ptr += sizeof(uint16_t), user, ulen);
    *(uint16_t*)(ptr += ulen) = plen;
    memcpy(ptr += sizeof(uint16_t), password, plen);
    ptr += plen;

    int bsize = ptr - buffer;




  //  printf("\nptr %u\n", ((uint16_t*)ptr));
//    printf("ptr2 %u\n", ptr);

   // exit(2);

    //encode
 //   *(uint16_t*)ptr = ulen;
 //   *(uint16_t*)ptr = ulen;

    hsec_response_t resp;
    int rv = 0;
    if((rv = SendData(hs_authenticate, 100, buffer, bsize, &resp))){
        return 3;
    }

    authenticated = 1;

   // printf(resp.data);

//    free(resp.pointer);

    if(getAccountData()) return 4;

    return 0;
}
