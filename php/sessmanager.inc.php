<?php

require_once("config.inc.php");

$hs_ack = 0x0;
$hs_renewkey = 0x1;
$hs_terminatesession = 0x2;
$hs_last = $hs_terminatesession;

function pq($err){ //print and quit
	print("Failure!" . pack("S", $err));
	exit(1);
}

function olog($string){
	$fp = fopen("debug.log", "ab");
	fprintf($fp, $string);
	fclose($fp);
}

function pqe($err, $key, $iv){ //print encrypted and quit
	$data = "Failure!" . pack("S", $err);
	print(openssl_encrypt($data,  "bf-ofb", $key, OPENSSL_RAW_DATA, $iv));
	exit(1);
}

	$lasterror = 0;

function fsize($fp){
	fseek($fp,0, SEEK_END);
	$size = ftell($fp);
	fseek($fp,0, SEEK_SET);
	return $size;
}

function session_needskeychange($session, $conn){
	global $lasterror, $keyexpiration;
	if(!($result = $conn->query("SELECT sessid FROM active_sessions WHERE UNIX_TIMESTAMP(date) + $keyexpiration <= UNIX_TIMESTAMP() AND sessid = '". $conn->real_escape_string($session) ."' LIMIT 1"))) {
	$lasterror = 16;
	return 0;
	}
	while($row = $result->fetch_assoc()){
		return 1; //it does
	}
	return 0; //it doesn't
}

function sess_initiator_decrypt(){
	global $privatekeypath, $lasterror;

if(!isset($_POST['data'])) {
$lasterror = 1;
return -1;
}


$data = base64_decode($_POST['data']);

//$data = file_get_contents("binout.bin");//base64_decode('');

$fp = fopen($privatekeypath, "rb");
if(!$fp) {
$lasterror = 2;
return -2;
}
$keydata = fread($fp, fsize($fp));
fclose($fp);

$key = openssl_get_privatekey($keydata);

$result = -3;
openssl_private_decrypt($data, $result, $key, OPENSSL_PKCS1_OAEP_PADDING);

if($result < 0 || strlen($result) > 128) {
$lasterror = 3;
return -4;
}
$keyivpair = explode(".", $result);
return $keyivpair;
}

function session_updateadate($session, $conn){
	global $lasterror;
		if(!($result = $conn->query("UPDATE `active_sessions` SET `adate` = FROM_UNIXTIME(UNIX_TIMESTAMP()) WHERE `sessid` = '".$conn->real_escape_string($session)."' LIMIT 1;"))) {
	$lasterror = 11;
	return 1;
	}
	return 0;
}

function session_updateiv($session, $newiv, $conn){
	global $lasterror;
		if(!($result = $conn->query("UPDATE `active_sessions` SET `iv` = '".$conn->real_escape_string(base64_encode($newiv))."' WHERE `sessid` = '".$conn->real_escape_string($session)."' LIMIT 1;"))) {
	$lasterror = 11;
	return 1;
	}
	return session_updateadate($session, $conn);
}

function session_updatekey($session, $newkey, $conn){
	global $lasterror;
		if(!($result = $conn->query("UPDATE `active_sessions` SET `bfkey` = '".$conn->real_escape_string(base64_encode($newkey))."', `date` = FROM_UNIXTIME(UNIX_TIMESTAMP()) WHERE `sessid` = '".$conn->real_escape_string($session)."' LIMIT 1;"))) {
	$lasterror = 11;
	return 1;
	}
	return 0;
}

function session_deletesession($session, $conn){
	global $lasterror;
		if(!($result = $conn->query("DELETE FROM `active_sessions` WHERE `sessid` = '".$conn->real_escape_string($session)."' LIMIT 1;"))) {
	$lasterror = 15;
	return 1;
	}
	return 0;
}

function session_deletesession2($session, $ss, $conn){
	global $lasterror;
		if(!($result = $conn->query("DELETE FROM `active_sessions` WHERE `sessid` = '".$conn->real_escape_string($session)."' AND `ss` = '".$conn->real_escape_string($ss)."' LIMIT 1;"))) {
	$lasterror = 17;
	return 1;
	}
	if(!mysqli_affected_rows($conn)){
	$lasterror = 18;
	return 2;
	}
	return 0;
}

//associate session with account
function session_associate($session, $aa, $conn){
	global $lasterror;
		if(!($result = $conn->query("UPDATE `active_sessions` SET `aa` = '".$conn->real_escape_string($aa)."', `adate` = FROM_UNIXTIME(UNIX_TIMESTAMP()) WHERE `sessid` = '".$conn->real_escape_string($session)."' LIMIT 1;"))) {
	$lasterror = 21;
	return 1;
	}
	return 0;
}

function bf_encrypt($data, $key, $iv){
	//$iv2 = $iv;
//	fprintf($fp, "ivv = %s\n", base64_encode($iv)); //debug
	$result = openssl_decrypt($data, "bf-ofb", $key, OPENSSL_RAW_DATA, $iv);
//	fprintf($fp, "ivv2 = %s\n", base64_encode($iv)); //debug
	//session_updateiv($session, $iv, $conn);
	return $result;
}



function session_retrievekeyivpair($sessid, $conn){
	global $lasterror;
	if(!($result = $conn->query("select bfkey, iv from active_sessions where sessid = '".$conn->real_escape_string($sessid)."' LIMIT 1"))) {
	$lasterror = 4;
	return 0;
	}
	$keyivpair = array();
	while($row = $result->fetch_assoc()){
		$keyivpair[0] = $row['bfkey'];
		$keyivpair[1] = $row['iv'];
		return $keyivpair;
	}
	$lasterror = 8;
	return 0; //it doesn't
}

function session_getassociatedaccount($sessid, $conn){
	global $lasterror;
	if(!($result = $conn->query("select aa from active_sessions where sessid = '".$conn->real_escape_string($sessid)."' LIMIT 1"))) {
	$lasterror = 22;
	return 0;
	}
	while($row = $result->fetch_assoc()){
		return $row['aa'];
	}
	$lasterror = 22;
	return 0; //it doesn't
}

function session_exists($sessid, $conn){
	global $lasterror;
	if(!($result = $conn->query("select sessid from active_sessions where sessid = '".$conn->real_escape_string($sessid)."' LIMIT 1"))) {
	$lasterror = 4;
	return 0;
	}
//	if(!($result = $conn->query("select sessid from active_sessions"))) return 1;
	while($row = $result->fetch_assoc()){
	return 1; //it does
	}
	return 0; //it doesn't
}

function session_ip($sessid, $conn){
	global $lasterror;
	if(!($result = $conn->query("select clientip from active_sessions where sessid = '".$conn->real_escape_string($sessid)."' LIMIT 1"))) {
	$lasterror = 4;
	return 0;
	}
//	if(!($result = $conn->query("select sessid from active_sessions"))) return 1;
	while($row = $result->fetch_assoc()){
	return $row['clientip'];
	}
	return 0; //it doesn't
}

function delete_expired($conn){
//delete inactive for more than 5 minutes
global $sessioninactivitylimit;
if(!$conn->query("DELETE FROM active_sessions WHERE UNIX_TIMESTAMP(adate) + $sessioninactivitylimit <= UNIX_TIMESTAMP()")) { $lasterror = 6; return 2;}
return 0;
}

function count_by_ip($conn, $ip){
//delete inactive for more than 5 minutes
if(!($result = $conn->query("SELECT COUNT(clientip) FROM active_sessions WHERE clientip = '".$conn->real_escape_string($ip)."'"))) { $lasterror = 6; return 2;}
while($row = $result->fetch_assoc()){
	//print_r($row);
	return $row['COUNT(clientip)'];
}
return 0;
}

function db_connect(){
global $dbhost, $dbuser, $dbpass, $dbname, $lasterror;
$conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
if(!$conn) {
$lasterror = 5;
return 0;
}
return $conn;
}

function insert_session(&$sessid, $bfkey, $iv, $clientip, &$ss, $conn){
global $dbhost, $dbuser, $dbpass, $dbname, $lasterror, $maxsessionsperip;

if(delete_expired($conn)) {
$lasterror = 2;
return 2;
}

//if(return count_by_ip($conn, $clientip));

if(count_by_ip($conn, $clientip) >= $maxsessionsperip) {
	$lasterror = 7;
	return 4;
}

while(session_exists($sessid, $conn)){ //session already exists, so generate another session id
	$sessid = uniqid("", true);
//print $sessid;
}

$ss = "";

for($i = 0; $i < 8; $i++){
	$ss .= mt_rand(0, 9);	
}

//insert
if(!$conn->query("INSERT INTO `active_sessions` (`sessid`, `bfkey`, `iv`, `clientip`, `date`, `adate`, `ss`) VALUES ('".$conn->real_escape_string($sessid)."', '".$conn->real_escape_string($bfkey)."', '".$conn->real_escape_string($iv)."', '".$conn->real_escape_string($clientip)."', FROM_UNIXTIME(UNIX_TIMESTAMP()), `date`, '".$ss."')")) { $lasterror = 6; return 2;}

return 0;
}

?>