<?php

$AccountsTable = "accounts";

class Accounts {
	public $id;
	public $username;
	public $passwordhash;
	public $salt;
	public $email;
	public $t; //table name
	public $type;
	
	public function __construct(){
		global $AccountsTable;
		$this->t = $AccountsTable;
	}
	
	static function findFirst($condition){
		global $conn, $AccountsTable;
		if(!($result = $conn->query("SELECT * FROM accounts WHERE $condition LIMIT 1"))) return 19;
		
			$row = $result->fetch_assoc();
			if(isset($row)){
				$n = new Accounts();
				$n->id = $row['id'];
				$n->username = $row['username'];
				$n->password = $row['password'];
				$n->salt = $row['salt'];
				$n->email = $row['email'];
				$n->type = $row['type'];
				
				return $n;
			}
			return 20;
	}
	
	static function associateWithSession($sessid){
		global $conn;
	}

}

?>