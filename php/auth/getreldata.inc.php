<?php
	require_once("accounts.inc.php");
	if(!($accid = session_getassociatedaccount($sessid, $conn))) pq(23);
	
	if(!is_a($acc = Accounts::findFirst("id = $accid"), "Accounts")) pq(24);
	
	/* account id (signed integer) */
	$response =  pack("i", $acc->id);
	
	/* account username size (unsigned short) + account username */
	$response .= pack("S", strlen($acc->username)) . $acc->username;
	
	/* account email size {unsigned short) + account email */
	$response .= pack("S", strlen($acc->email)) . $acc->email;
	
	/* account type (unsigned char) */
	$response .= pack("C", $acc->type);
	
?>