In case you wish to add new fields to the account class you must edit the following files:

userauth.h -> add the fields to the class definition
userauth.cpp -> add the fields in the decoding process in getAccountData()


auth/getreldata.inc.php -> add the new fields to send
auth/accounts.inc.php -> add the fields to the class definition, and add to findFirst

of course you also need to add the database fields.
