<?php

$production = 1; //this means, units will load as production code

//error_reporting(0);
//$fp = fopen("debug2.log", "wb"); //debug

require_once("sessmanager.inc.php");
require_once("commands.inc.php");

//communication handling functions

$conn = db_connect();
if($lasterror) pq($lasterror);

if(!isset($_POST['sessid'])) pq(9);
$sessid = $_POST['sessid'];
if(!isset($_POST['data'])) pq(10);
$data = base64_decode($_POST['data']);

$keyivpair = session_retrievekeyivpair($sessid, $conn);
if($lasterror) pq($lasterror);

//fprintf($fp,  "base64keyout = %s\n", $keyivpair[1]); //debug


$iv = base64_decode($keyivpair[1]);
$key = base64_decode($keyivpair[0]);
//print "caca";

$decrypted = openssl_encrypt($data, "bf-ofb", $key, OPENSSL_RAW_DATA, $iv);
//validation
if(!(substr($decrypted, 0, 4) == "Cli!")) pq(12);
$command = unpack("S", substr($decrypted, 4, 2));
//olog($command[1] . "\n");
$newiv = substr($decrypted, 6, 8);
session_updateiv($sessid, $newiv, $conn);
//print $decrypted . "\0";

//print $command[1];

$resultcode = session_needskeychange($sessid, $conn);
if($lasterror) pq($lasterror);
$response = ""; //empty response

/* available variables for included files:
$sessid: session unique id
$decrypted: raw decrypted data, relevant data begins at address 18
$command: the command sent
$response: what the server will respond; THIS MAY BE CHANGED
$resultcode: $resultcode & 1 indicates wether or not the key should be changed to the client 
$key: the current bf key for this session
$conn: the current mysql connection
*/ 

require_once("auth/auth_stack.inc.php");

switch($command[1]){
	case $hs_ack: //space out
//	$response = "lulz0r\0";
	break;
	case $hs_renewkey: //hs_renewkey
		require_once("actions/renewkey.inc.php");
	break;
	case $hs_terminatesession: //hs_terminatesession
		require_once("actions/terminatesession.inc.php");
	break;
	/* auth */
	case $hs_authenticate:
		require_once("auth/authenticate.inc.php");
	break;
	case $hs_getrelevantdata:
		require_once("auth/getreldata.inc.php");
	break;
	/* end auth */
	case 0xfe: //ping/pong test
		$response = pack("L", unpack("L", substr($decrypted, 14, 4))[1] + 1);
	break;
	default:
		pq(13);
	break;
}

$strong = false;
$ni = openssl_random_pseudo_bytes(8, $strong);
if(!$strong) pq(14); 

//print response
print bf_encrypt("Success!" . pack("S", $resultcode) . $ni . $response,  $key, $newiv);

if(session_exists($sessid, $conn))
session_updateiv($sessid, $ni, $conn);

mysqli_close($conn);

return 0;
?>