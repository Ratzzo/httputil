<?php

//$fp = fopen("debug.log", "wb"); //debug

require_once("sessmanager.inc.php");

$conn = db_connect();
if($lasterror) pq($lasterror);

$keyivpair = sess_initiator_decrypt();
if($lasterror) pq($lasterror);

if(isset($keyivpair[3]) && isset($keyivpair[4])){
	//session is being retaken
	session_deletesession2(base64_decode($keyivpair[4]), base64_decode($keyivpair[3]), $conn);
	if($lasterror) pq($lasterror);
	$unique_id = base64_decode($keyivpair[4]);
}
else
{
	$unique_id = uniqid("", true);
}
insert_session($unique_id, $keyivpair[0], $keyivpair[2] /* insert second iv */, $_SERVER['REMOTE_ADDR'], $ss, $conn);
if($lasterror) pq($lasterror);

print bf_encrypt("Success!\0\0".$ss.pack("C",strlen($unique_id))."$unique_id", base64_decode($keyivpair[0]), base64_decode($keyivpair[1]) /* use first iv */);

//fprintf($fp, "iv changed? = %s\n", base64_encode($iv));

mysqli_close($conn);

//fclose($fp); //debug

return 0;
?>