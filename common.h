#ifndef DCOMMON_H
#define DCOMMON_H

#include <stdint.h>
#include <stdio.h>

/* common memory data structures */

#pragma pack(push, 1)

typedef struct {
    uint32_t size;
    char *data;
} datasize32_t;

typedef struct {
    uint32_t size : 24;
    char *data;
} datasize24_t;

typedef struct {
    uint16_t size;
    char *data;
} datasize16_t;

typedef struct {
    uint8_t size;
    char *data;
} datasize8_t;

#define pointstrucdef(bits) typedef struct point##bits##_t{ \
    union { \
        uint##bits##_t ux; \
        int##bits##_t x; \
    }; \
    union { \
        uint##bits##_t uy; \
        int##bits##_t y; \
    }; \
} point##bits##_t;

pointstrucdef(8);
pointstrucdef(16);
pointstrucdef(32);

#undef pointstrucdef

typedef struct lineequation_t {
    float slope;
    float yintercept;
} lineequation_t;

/* error handler structure */

typedef struct {
    uint32_t errorcode, line;
    char **errortable;
    char *sourcefile;
} errorhandler_t;

typedef int (*common_globalerror_callback)(errorhandler_t *source);


#pragma pack(pop)

extern common_globalerror_callback globalerror_callback;

//char *translateerror(errorhandler_t *errorhandler);

#define translateerror(errorhandler) (errorhandler)->errortable[(errorhandler)->errorcode]
#define printlasterror(handler) printf("%s\n", translateerror(handler))
#endif
