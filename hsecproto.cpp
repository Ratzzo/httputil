#include "hsecproto.h"
#include "main.h"

const char *hs_public_error_list[] =
{
    "Success.",
    "",
    "",
    "Decryption failed.",
    "",
    "",
    "",
    "Maximum numbers of sessions reached.",
    "",
    "",
    "",
    "",
    ""
};

#define HS_CLI_ERROR 0xffffffff


HSec::HSec(hutil_t *hut, const char *baseurl, const char *sessioninitiator, const char *comhandler)
{
    this->baseurl.assign(baseurl);
    this->sessioninitiator.assign(sessioninitiator);
    this->comhandler.assign(comhandler);
    RSA_Public_Key.data = (char*)malloc(1);
    RSA_Public_Key.size = 0;
    lasthserror = 0;
    iv_toggle = 0;
    ivs[0] = iv;
    ivs[1] = iv2;
    memset(&lastresponse, 0, sizeof(lastresponse));
    memset(&savedresponse, 0, sizeof(savedresponse));
    sessionactive = 0;
    busycallback = 0;
    hu = hut;
}

int HSec::SetPublicKey(const char *key, int size)
{
    lasthserror = HS_CLI_ERROR;
    if(RSA_Public_Key.data)
        RSA_Public_Key.data = (char*)realloc(RSA_Public_Key.data, size);
    if(!RSA_Public_Key.data) return 1;
    memcpy(RSA_Public_Key.data, key, size);
    RSA_Public_Key.size = size;
    lasthserror = 0;
    return 0;
}

uint32_t HSec::GetLastHSError()
{
    uint32_t le = lasthserror;
    lasthserror = 0;
    return le;
}

const char *HSec::TranslateHSError(uint32_t error)
{
    if(error == HS_CLI_ERROR)
        return "Client-side error.";
    return hs_public_error_list[error];
}


//renews key and iv

#define incbuffer(b, ss, s){ \
    b += s; \
    ss += s; \
}

int HSec::SendData(uint16_t command, int maxsize, const char *data, int datasize, hsec_response_t *decryp, int recursive)
{
 //printf("addr %x\n", data);
    //args
//#define command hs_renewkey
//#define maxsize 10000
//#define ddata "asdasdasd"
//#define ddatasize 9
    hutil_downdata_t dd;
    int tempbufflen = 0;
    int bufflen = 0;
    //char bu[8192];
    char *buffer;
    std::string postdata;
    unsigned char ivtemp[8];
    lasthserror = HS_CLI_ERROR;
    bufflen = datasize + 14;
    char *bu = (char*)malloc(bufflen);
    if(decryp) memset(decryp, 0, sizeof(*decryp));
    if(!bu) return 1;
    buffer = bu;
    memset(buffer, 0, bufflen);

    //compose message

//    #define str "Clie"
    sprintf(buffer, "%s", "Cli!"); //Cli! header
    incbuffer(buffer, tempbufflen, 4); //increase by size of "Cli!"
    *(uint16_t*)buffer = command/*command */; //command
    incbuffer(buffer, tempbufflen, sizeof(uint16_t));
    memcpy(buffer, ivs[iv_toggle^1], 8); //new_iv
    incbuffer(buffer, tempbufflen, 8);
    if(data){ //otherwise data is irrelevant
    memcpy(buffer, data, datasize);
    incbuffer(buffer, tempbufflen, datasize);
    }
//    printf("buffer %s\n", bu);

    char *huresult = 0;
    memcpy(ivtemp, ivs[iv_toggle], 8); //store current iv
    huresult = hutil_urlencodedbase64bfencrypt(&key, (unsigned char*)bu, tempbufflen, ivs[iv_toggle]);
    free(bu);

    postdata = "sessid=" + sessid + "&data=" + huresult;
    if(huresult) iv_toggle ^= 1;
    else
        return 2;


    int rval = 0;
    if(busycallback){
            std::string tt = (baseurl + comhandler);
        if(!(rval = hutil_performasyncrequest(hu, tt.c_str(), &dd, (char*)postdata.c_str())))
        while(!dd.finished || dd.rval < 0)
            dd.forcetermination = busycallback(&dd);
            busycallback(&dd);
        rval = dd.rval;
       // printf("RVAL = %i", rval);
    }
    else
    rval = hutil_performblockingrequest(hu, (baseurl + comhandler).c_str()/*possible memory leak */, &dd, (char*)postdata.c_str());



    if(rval)
    {
        if(huresult) iv_toggle ^= 1;
        //rollback iv
        memcpy(ivs[iv_toggle], ivtemp, 8);
        free(huresult);
        dprint("hutil_performrequest = %i\n", rval);
        return 3;
    }
    //printf(dd.data);

    free(huresult);

    //printf("%s\n", dd.data);

    if(dd.size < 10 || dd.size > maxsize)
    {
        free(dd.data);
        return 4;
    };

    char *decrypted = (char*)malloc(dd.size);
    if(!decrypted)
    {
        free(dd.data);
        return 5;
    }

    if(!memcmp(dd.data, "Failure!", 8))
    {
//        dprint("lasthserror = %u\n", lasthserror);
       // if(lasthserror == 12){
            StartSession();
        //}
        lasthserror = *(uint16_t*)(dd.data + 8);
        free(decrypted);
        free(dd.data);
        return 6;
    }

    int i = 0;

    BF_ofb64_encrypt((const unsigned char*)dd.data, (unsigned char *)decrypted, dd.size, &key, ivs[iv_toggle], &i);
    free(dd.data);
    iv_toggle ^= 1;

    buffer = decrypted;

    //verify integrity
    if(memcmp(buffer, "Success!", 8))
    {
        free(decrypted);
        return 7;
    }

    //dprint("asd = %s\n", buffer);

    //get new iv from server.

    buffer += 10;
    memcpy(ivs[iv_toggle], buffer, 8);
    if(lastresponse.pointer && !recursive){
       free(lastresponse.pointer);
       lastresponse.pointer = 0;
    }
    lastresponse.pointer = decrypted;
    lastresponse.code = *(uint16_t*)(decrypted + 8);
    lastresponse.length = dd.size-18;
    lastresponse.data = decrypted+18;

    if(!recursive)
        savedresponse = lastresponse;

    if(lastresponse.code & HSEC_RENEWKEY && !recursive){
        //renew the key with a recursive call;

        //generate a new key;
        unsigned char nk[32];
        hsec_response_t resp;
        int rv = 0;
        if(!RAND_bytes(nk, sizeof(nk))) return 8;
        dprint("nk = ");
        hutil_hexdump(nk, 32);
        //send the key along with the renewkey command
        if((rv = SendData(hs_renewkey, 100, (char*)nk, sizeof(nk), &resp, 1))){
                dprint("send data failed %u\n", rv);
            return 9;
        }
        if(resp.code == 1) {
                dprint("key didn't change\n");
               // free(lastresponse.pointer);
                return 10;
        }
        dprint("%u\n", resp.code);
        //if everything goes right update key
        BF_set_key(&key, sizeof(nk), nk);
        memcpy(k, nk, sizeof(k));
        free(lastresponse.pointer);
        lastresponse = savedresponse;


    }


    if(decryp)
    *decryp = lastresponse;
//    free(decrypted);
//   printf("current iv:");
//    hutil_hexdump(ivs[iv_toggle], 8);


    lasthserror = 0;
    return 0;
}

#undef incbuffer

int HSec::StartSession()
{
    lasthserror = HS_CLI_ERROR;
    unsigned char bu[4096];
    unsigned char *buffer = bu;
    unsigned char temp[2048];
    if(!RAND_bytes(buffer, sizeof(bu))) return 1;


    //pack random key
    int pos = sizeof(k);
    for(unsigned int i = 0; i < sizeof(bu)/sizeof(k); i++)
    {
        for(int m = 0; m < (int)sizeof(k); m++)
        {
            buffer[m] ^= buffer[pos];
            pos++;
        }
    }
    memcpy(k, buffer, sizeof(k));
    BF_set_key(&key, sizeof(k), k);
    if(!RAND_bytes(iv, 8)) return 2;
    if(!RAND_bytes(iv2, 8)) return 3; //iv2 is used to determine the next iv

    //memcpy(buffer, k, sizeof(k));
    memset(temp, 0, sizeof(temp));
    memset(buffer, 0, sizeof(bu));

    int len = 0;
    apr_base64_encode((char*)temp, (const char*)k, sizeof(k));
    temp[len = strlen((char*)temp)] = '.';
    apr_base64_encode((char*)temp + len + 1, (const char*)ivs[iv_toggle], 8);
    temp[len = strlen((char*)temp)] = '.';
    apr_base64_encode((char*)temp + len + 1, (const char*)ivs[iv_toggle^1], 8);
    if(sessionactive){
        temp[len = strlen((char*)temp)] = '.';
        apr_base64_encode((char*)temp + len + 1, (const char*)ss, 8);
        temp[len = strlen((char*)temp)] = '.';
        apr_base64_encode((char*)temp + len + 1, (const char*)sessid.c_str(), sessid.size());
    }
    len = strlen((char*)temp);

    char *data = hutil_urlencodedbase64rsapublicencrypt((char*)RSA_Public_Key.data, RSA_Public_Key.size, (char*)temp, len);
    if(!data) return 9;
    //printf("%s\n", temp);
    //sprintf()
    hutil_downdata_t dd;
    char postdata[4096];
    memset(postdata, 0, sizeof(postdata));
    sprintf(postdata, "data=%s", data);

    int rval = 0;

    if(busycallback)
    {
        std::string tt = (baseurl + sessioninitiator);
        if(!hutil_performasyncrequest(hu, tt.c_str(), &dd, postdata))
        while(!dd.finished || dd.rval < 0){
            dd.forcetermination = busycallback(&dd);
            }
            busycallback(&dd);
            rval = dd.rval;
    }
    else
        rval = hutil_performblockingrequest(hu, (baseurl + sessioninitiator).c_str(), &dd, postdata);

        if(rval){
            free(data);
            return 8;
        }

    free(data);
    //printf("%s\n", postdata);
    // printf("---------------------------------------------------\n");
    //printf("%s\n", dd.data);


    //if(dd.size > 2048 || dd.size < 10) {free(dd.data); return 3;}

//    printf("local key");
//    hutil_hexdump(k, 32);

//    printf("local iv");
//    hutil_hexdump(iv, 8);

//    printf("remote key");
//    hutil_hexdump((unsigned char*)dd.data, 32);
//    return 3;

    if(!memcmp(dd.data, "Failure!", 8))
    {

        lasthserror = *(uint16_t*)(dd.data + 8);
        //printf("%u\n", lasthserror);
        free(dd.data);
        return 4;
        //printf("hs ret = %u\n", lasthserror);
    }

    memset(buffer, 0, sizeof(bu));

    int i = 0;
    int sessionstrsize = 0;
    //iv changes here, however there is no need to rollback the iv since if it changes here the whole initialization process fails
    BF_ofb64_encrypt((const unsigned char*)dd.data, buffer, dd.size, &key, ivs[iv_toggle], &i);
    if(data) iv_toggle ^= 1;


//    printf("local key");
//    hutil_hexdump(k, 32);

//    printf("local iv");
//    hutil_hexdump(ivs[iv_toggle], 8);

//    printf("%s\n", buffer);
    free(dd.data);
    if(memcmp((const char*)buffer, "Success!", 8))
        return 5;
    else
    {
        buffer += 8; //increment buffer by the size of "Success!"
        lasthserror = *(uint16_t*)(buffer);
        buffer += sizeof(uint16_t);
        memcpy(ss, buffer, sizeof(ss));
        buffer += sizeof(ss);
        sessionstrsize = *(uint8_t*)(buffer);
        buffer += sizeof(uint8_t);
    }

    sessid.assign((char*)buffer, sessionstrsize);
    //printf("sessid = %s\n", sessid.c_str());
    //printf("%s\n", sessid.c_str());
//    if(strlen((const char*)buffer + 9) >= sizeof(sessid)) 00 5;
//    sprintf(sessid, "%s", buffer + 9); //get sessid
    //printf("sessid %s\n", sessid);
    lasthserror = 0;
    sessionactive = 1;
    return 0;
}

int HSec::EndSession(){
    hsec_response_t resp;
    int ret = SendData(0x2, 100, 0, 0, &resp);
    //printf("%u\n", ret);
    if(!ret) sessionactive = 0;
    return ret;
}

HSec::~HSec()
{
    if(sessionactive) EndSession();
    if(lastresponse.pointer) free(lastresponse.pointer);
    if(RSA_Public_Key.data) free(RSA_Public_Key.data);
}
