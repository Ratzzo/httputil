#ifndef HSEC_PROTO
#define HSEC_PROTO

/** HSec Protocol */

#include <common.h>
#include <stdio.h>
#include <string>
#include <vector>
#include <openssl/rand.h>
#include <openssl/blowfish.h>
#include <openssl/err.h>
#include "httputil.h"
#include "base64/apr_base64.h"
//#include "debug.h"

#ifdef _DEBUG_
#define dprint(...) printf(__VA_ARGS__)
#else
#define dprint(...)
#endif

/*
all communication is performed over http, data is sent encrypted and base64 encoded.
all the data is handled by commands, the server responds with "Success!", and "Failure!" accordingly. both of them
followed by a two byte error code (uint16_t). Also, all blowfish encrypted data sent by the client starts with "Cli!" as a validation header.
*/


#pragma pack(push, 1)

typedef struct {
    uint8_t r[8]; //response
    uint16_t errorcode;
} HS_response_header_t;

enum HSec_ComMap {
/** commands */
    hs_ack = 0x0,
    hs_renewkey = 0x1,
    hs_terminatesession = 0x2,
    hs_end = 0xffff
};

#define hs_last 0x2

typedef struct {
    char *data;
    int length;
    uint16_t code;
    char *pointer; //internal use
} hsec_response_t;

typedef int (*hsec_busycallback_t)(hutil_downdata_t *data);

#define HSEC_RENEWKEY 1

class HSec{
protected:
    hutil_t *hu;
    unsigned char k[32];
    unsigned char iv[8];
    unsigned char iv2[8];
    unsigned char iv_toggle;
    unsigned char *ivs[2];
    unsigned char ss[8]; //shared secret in case the session is lost and must be reestablished. ie. Error 12 after CURLE_COULDNT_CONNECT.
    BF_KEY key;
    datasize32_t RSA_Public_Key;
    std::string sessid;
    hsec_response_t lastresponse, savedresponse;
    int sessionactive;
public:
    hsec_busycallback_t busycallback; //called on processing as a while(processing) { busycallback() } on nonzero return, the operation is aborted. NOTE: you need to put a sleep, select, etc here or the whole program will hang.
    uint32_t lasthserror;
    std::string baseurl;
    std::string sessioninitiator;
    std::string comhandler;

    HSec(hutil_t *hu, const char *baseurl, const char *sessioninitiator = "sessioninitiator.php", const char *comhandler = "comhandler.php");

    uint32_t GetLastHSError();

    const char *TranslateHSError(uint32_t error);

    int SendData(uint16_t command, int maxsize, const char *data, int datasize, hsec_response_t *decryp, int recursive = 0);

    int SetPublicKey(const char *key, int size);

    int StartSession();

    int EndSession();

    ~HSec();
};

#pragma pack(pop)

#endif // HSEC_PROTO
