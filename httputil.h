#ifndef HTTPUTIL_H
#define HTTPUTIL_H

#define CURL_STATICLIB

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <curl/curl.h>
#include <apr.h>
#include <apr_thread_proc.h>
#include <apr_pools.h>
#include "base64/apr_base64.h"
#include <common.h>
#include <openssl/rsa.h>
#include <openssl/blowfish.h>
#include <openssl/pem.h>
#include <openssl/bio.h>
#include <openssl/err.h>

//#include "debug.h"

#ifndef HUTIL_CACERTPATH
#define HUTIL_CACERTPATH "certs/ca-certificates.crt"
#endif // HUTIL_CACERTPATH

#ifndef HUTIL_POOLCLEANUP
#define HUTIL_POOLCLEANUP 1 //clean every 200 calls
#endif // HUTIL_CLEANUP

typedef struct {
    char *data;
    int size;
    int progress;
    int expectedtotal;
    int finished;
    int forcetermination;
    int rval;
} hutil_downdata_t;

typedef struct {
apr_pool_t *hutil_pool;
int _hutil_allocs;
CURL *curl_handle;
} hutil_t;

//initializes hutil
int hutil_initialize(hutil_t *hu);

//starts a new thread to download data
//dd.data must be freed afterwards
int hutil_performasyncrequest(hutil_t *hu, const char *url, hutil_downdata_t *dd, char *postfields = 0);

//perform a blocking download
//dd.data must be freed afterwards
int hutil_performblockingrequest(hutil_t *hu, const char *url, hutil_downdata_t *dd, char *postfields = 0);

//terminates hutil
int hutil_terminate(hutil_t *hu);

//encrypts data given der as byte array
//out->data must be freed afterwards if the function succeeds
int hutil_rsapublicencrypt(char *der, int derlen, const unsigned char *in, int insize, datasize32_t *out);

//encrypts data, and outputs it as a base64 null terminated string
//return value must be freed afterwards
char *hutil_base64rsapublicencrypt(char *der, int derlen, const char *in, int insize);

//encrypt data, encode it base64, and then encode as URL
//return value must be freed afterwards
char *hutil_urlencodedbase64rsapublicencrypt(char *der, int derlen, const char *in, int insize);

//encrypt data using blowfish ofb mode, then pack it as a base64 string
//returned string must be freed after usage
char *hutil_base64bfencrypt(BF_KEY *key, unsigned char *buffer, unsigned int buffersize, unsigned char *iv);

//encrypt data using blowfish ofb mode
//returned string must be freed after usage
unsigned char *hutil_bfencrypt(BF_KEY *key, unsigned char *buffer, unsigned int buffersize, unsigned char *iv, int *len);

char *hutil_urlencodedbase64bfencrypt(BF_KEY *key, unsigned char *buffer, unsigned int buffersize, unsigned char *iv);

void hutil_hexdump(unsigned char *buff, int size);

//encode url
char *hutil_urlencode(const char *src);
#endif // HTTPUTIL_H
