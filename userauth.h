#ifndef USERAUTH_H
#define USERAUTH_H

#include "hsecproto.h"
#include <string>

/* command allocations
these are relatives since it's stack based
*/

extern uint16_t hs_authenticate;
extern uint16_t hs_getrelevantdata;

#pragma pack(push, 1)

typedef struct {
int id;
uint8_t type;
std::string username;
std::string email;
//add more
} ua_accountdata_t;

class UserAuth : public HSec {
public:
    ua_accountdata_t ad;
    int authenticated;
    UserAuth(hutil_t *hu, const char *baseurl, const char *sessioninitiator = "sessioninitiator.php", const char *comhandler = "comhandler.php");
    //authenticates this session (associates the session with an account). it's your responsibility to zero the password afterwards.
    int authenticate(const char *user, const char *password);
    //updates local copy of relevant account data (the password will never be retrieved)
    int getAccountData();

};

#pragma pack(pop)

#endif // USERAUTH_H
